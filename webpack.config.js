const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  entry: './src/index.js',
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, 'dist'),
  },
  plugins:[
      new MiniCssExtractPlugin(),
      new HtmlWebpackPlugin({
          template: './src/index.html'
      })
  ],
  module:{
      rules:[
          {
              test:/\.s[ac]ss$/i,
              use:[
                  MiniCssExtractPlugin.loader,
                  'css-loader',
                  'sass-loader',
              ],
          },
          {
              test: /\.(png|svg|jpg|gif)$/i,
              loader:'file-loader',
              options: {
                outputPath: 'images',
              },
          },
          {
            test: /\.html$/i,
            loader: 'html-loader',
          },    
      ],
  },
  watch:true
};