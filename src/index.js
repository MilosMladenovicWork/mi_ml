import './index.html'
import './styles/global.sass'

class App {
    constructor(){
        this.dom = {
            logInButton: document.querySelector('.log-button'),
            editContentButton: document.querySelectorAll('.info-container [class^=info-change-button]'),
            editContentPopup: document.querySelectorAll('.info-container .edit-field-popup'),
            cancelEditContentButton: document.querySelectorAll('.info-container .cancel-changes'),
            saveEditContentButton: document.querySelectorAll('.info-container .save-changes'),
            inputValueElements1: document.querySelectorAll('.user-name'),
            inputValueElements2: document.querySelectorAll('.web-address'),
            inputValueElements3: document.querySelectorAll('.tel-number'),
            inputValueElements4: document.querySelectorAll('.address'),
            inputs: document.querySelectorAll('.edit-field-popup > input'),
            inputLabels: document.querySelectorAll('.edit-field-popup > label'),
            defaultValues: document.querySelectorAll('.info > p > span'),
            infoContainers: document.querySelectorAll('.info-container'),
            nameInput: document.querySelectorAll('.edit-field-popup > input')[0],
            slides: document.querySelectorAll('.swipable-container > .slide'),
            slider: document.querySelector('.swipable-container'),
            navButtons: document.querySelectorAll('.navigation-bar li > button'),
            navMenu: document.querySelector('.navigation-bar'),
            //mobile popups
            sectionEditContentButton: document.querySelectorAll('.information-section > [class^=info-change-button]'),
            sectionEditContentPopup: document.querySelectorAll('.information-section > .edit-field-popup'),
            sectionInputs: document.querySelectorAll('.information-section > .edit-field-popup input'),
            sectionCancelEditContentButton: document.querySelectorAll('.information-section > .edit-field-popup .cancel-changes'),
            sectionSaveEditContentButton: document.querySelectorAll('.information-section > .edit-field-popup .save-changes'),
            sectionInputLabels: document.querySelectorAll('.information-section > .edit-field-popup label'),
        }
        this.state = {
            logInButton: 'LOG OUT',
            editContentButton: undefined,
            editContentPopup: undefined,
            inputs: (() => {
                const inputsArray = []
                if(this.dom.defaultValues){
                    this.dom.inputs.forEach((input, index) => {
                        inputsArray.push({inputValue:this.dom.defaultValues[index].innerText, oldInputValue: this.dom.defaultValues[index].innerText})})
                }
                return inputsArray
            })(),
            activeSlide: 0,
            usingMobile: false,
            //mobile popups
            sectionEditContentButton: undefined,
            sectionEditContentPopup: undefined,
            sectionInputs: (() => {
                const inputsArray = []
                if(this.dom.defaultValues){
                    this.dom.sectionInputs.forEach((input, index) => {
                        if(index === 0){
                            return inputsArray.push({inputValue:this.dom.defaultValues[0].innerText.split(' ')[0], oldInputValue: this.dom.defaultValues[0].innerText.split(' ')[0]
                        })
                    }else if(index === 1){
                        return inputsArray.push({inputValue:this.dom.defaultValues[0].innerText.split(' ').slice(1, this.dom.defaultValues[0].innerText.split(' ').length).join(' '), oldInputValue: this.dom.defaultValues[0].innerText.split(' ').slice(1, this.dom.defaultValues[0].innerText.split(' ').length).join(' ')
                    })
                }
                inputsArray.push({inputValue:this.dom.defaultValues[index - 1].innerText, oldInputValue: this.dom.defaultValues[index - 1].innerText})
            }) 
        }
        console.log(inputsArray)
                return inputsArray
            })()
        }
        this.bindEvents()
        this.render()
    }

    render(){
        this.dom.logInButton.innerText = this.state.logInButton
        
        if(this.state.editContentPopup !== undefined){
            this.dom.editContentPopup[this.state.editContentPopup].classList.add('popup--state-active') 
        }
        if(this.state.sectionEditContentPopup !== undefined){
            this.dom.sectionEditContentPopup[this.state.sectionEditContentPopup].classList.add('popup--state-active')
        }

        this.dom.inputValueElements1.forEach(inputValueElement => inputValueElement.innerText = this.state.inputs[0].inputValue)
        this.dom.inputValueElements2.forEach(inputValueElement => inputValueElement.innerText = this.state.inputs[1].inputValue)
        this.dom.inputValueElements3.forEach(inputValueElement => inputValueElement.innerText = this.state.inputs[2].inputValue)
        this.dom.inputValueElements4.forEach(inputValueElement => inputValueElement.innerText = this.state.inputs[3].inputValue)
        
        this.dom.inputs.forEach((input, index) => input.value = this.state.inputs[index].inputValue)
        
        //only mobile
        if(this.state.usingMobile){
            this.dom.sectionInputs.forEach((input, index) => input.value = this.state.sectionInputs[index].inputValue)
            this.dom.inputValueElements1.forEach(inputValueElement => inputValueElement.innerText = `${this.state.sectionInputs[0].inputValue} ${this.state.sectionInputs[1].inputValue}`)
            this.dom.inputValueElements2.forEach(inputValueElement => inputValueElement.innerText = this.state.sectionInputs[2].inputValue)
            this.dom.inputValueElements3.forEach(inputValueElement => inputValueElement.innerText = this.state.sectionInputs[3].inputValue)
            this.dom.inputValueElements4.forEach(inputValueElement => inputValueElement.innerText = this.state.sectionInputs[4].inputValue)
        }
        
    }

    bindEvents(){
        this.dom.logInButton.addEventListener('click', this.changeLogInStatus.bind(this))
        this.dom.editContentButton.forEach((button, index) => button.addEventListener('click', this.toggleEditPopup.bind(this, index)))
        this.dom.sectionEditContentButton.forEach((button, index) => button.addEventListener('click', this.toggleSectionEditPopup.bind(this, index)))
        this.dom.cancelEditContentButton.forEach((cancelButton, index) => cancelButton.addEventListener('click', this.cancelEdit.bind(this, index)))
        this.dom.saveEditContentButton.forEach(saveButton => saveButton.addEventListener('click', this.saveEdit.bind(this)))
        this.dom.inputs.forEach((input, index) => {
            input.addEventListener('input', this.inputHandler.bind(this, index))
        })
        this.dom.navButtons.forEach((button, index) => button.addEventListener('click', this.navigateToSection.bind(this, index)))
        this.dom.slider.addEventListener('touchmove', this.swipeDetection.bind(this))
        this.dom.slider.addEventListener('touchstart', this.swipeDetection.bind(this))
        this.dom.navMenu.addEventListener('touchmove', this.swipeDetection.bind(this))
        this.dom.navMenu.addEventListener('touchstart', this.swipeDetection.bind(this))
        
        this.dom.sectionEditContentButton.forEach((button, index) => button.addEventListener('click', this.toggleSectionEditPopup.bind(this, index)))
        this.dom.sectionCancelEditContentButton.forEach((button, index) => button.addEventListener('click', this.cancelSectionEdit.bind(this, index)))
        this.dom.sectionSaveEditContentButton.forEach((button) => button.addEventListener('click', this.saveSectionEdit.bind(this)))
        this.dom.sectionInputs.forEach((input, index) => {
            input.addEventListener('input', this.sectionInputHandler.bind(this, index))
        })
    }
    
    changeLogInStatus(event){
        event.preventDefault();
        if(this.state.logInButton == 'LOG IN'){
            this.state.logInButton = 'LOG OUT'
        }else{
            this.state.logInButton = 'LOG IN'
        }
        this.render()
    }

    toggleEditPopup(index, event){
        event.preventDefault()
        this.state.inputs.forEach((input, index) => {
            if(input.inputValue != ''){
                this.dom.inputLabels[index].classList.add('label--floating')
            }else{
                this.dom.inputLabels[index].classList.remove('label--floating')
            }
        })
        if(this.state.editContentPopup !== undefined){
            //remove active class from earlier edited popup 
            this.dom.editContentPopup[this.state.editContentPopup].classList.remove('popup--state-active')
            //restore older value from earlier edited popup
            this.state.inputs.forEach((input, index) =>  this.state.inputs[index].inputValue = this.state.inputs[index].oldInputValue)
        }
        if(index !== undefined){
            //save old input value if user decides to cancel input
            this.state.inputs[index].oldInputValue = this.state.inputs[index].inputValue
        }
        this.state.editContentButton = index
        this.state.editContentPopup = index
        this.render()
    }

    toggleSectionEditPopup(index, event){
        event.preventDefault()
        this.state.sectionInputs.forEach((input, index) => {
            if(input.inputValue != ''){
                this.dom.sectionInputLabels[index].classList.add('label--floating')
            }else{
                this.dom.sectionInputLabels[index].classList.remove('label--floating')
            }
        })
        this.state.usingMobile = true
        this.dom.sectionEditContentButton[index].classList.add('info-change-button--unavailable')
        this.state.sectionEditContentButton = index
        this.state.sectionEditContentPopup = index
        this.render()
    }
    
    cancelSectionEdit(index, event){
        event.preventDefault()
        this.dom.sectionEditContentButton[index].classList.remove('info-change-button--unavailable')
        this.dom.sectionEditContentPopup[this.state.sectionEditContentPopup].classList.remove('popup--state-active')
        if(this.state.sectionInputs){
            this.state.sectionInputs.forEach((input, index) => {
                return this.state.sectionInputs[index].inputValue = this.state.sectionInputs[index].oldInputValue
            })
        }
        this.state.sectionEditContentButton = undefined
        this.state.sectionEditContentPopup = undefined
        this.render()
    }
    
    saveSectionEdit(event){
        event.preventDefault()
        this.dom.sectionEditContentButton[this.state.sectionEditContentButton].classList.remove('info-change-button--unavailable')
        this.dom.sectionEditContentPopup[this.state.sectionEditContentPopup].classList.remove('popup--state-active')
        this.state.sectionInputs.forEach((input, index) => {
            return this.state.sectionInputs[index].oldInputValue = this.state.sectionInputs[index].inputValue
        })

        this.state.sectionEditContentButton = undefined
        this.state.sectionEditContentPopup = undefined
        
        this.render();
    }
    
    sectionInputHandler(index ,event){
        this.state.sectionInputs[index].inputValue = event.target.value
        if(this.state.sectionInputs[index].inputValue != ''){
            this.dom.sectionInputLabels[index].classList.add('label--floating')
        }else{
            this.dom.sectionInputLabels[index].classList.remove('label--floating')
        }

        this.dom.sectionInputs[index].value = this.state.sectionInputs[index].inputValue

        this.render()
    }
    cancelEdit(index, event){
        event.preventDefault()
        this.dom.editContentPopup[this.state.editContentPopup].classList.remove('popup--state-active')
        this.state.inputs[index].inputValue = this.state.inputs[index].oldInputValue

        this.state.editContentButton = undefined
        this.state.editContentPopup = undefined

        this.render();
    }

    saveEdit(event){
        event.preventDefault()
        this.dom.editContentPopup[this.state.editContentPopup].classList.remove('popup--state-active')

        this.state.editContentButton = undefined
        this.state.editContentPopup = undefined

        this.render();
    }

    inputHandler(index ,event){
        this.state.inputs[index].inputValue = event.target.value

        if(this.state.inputs[index].inputValue != ''){
            this.dom.inputLabels[index].classList.add('label--floating')
        }else{
            this.dom.inputLabels[index].classList.remove('label--floating')
        }

        this.dom.inputs[index].value = this.state.inputs[index].inputValue

        this.render()
    }

    navigateToSection(index, event){
        this.state.activeSlide = index
        this.dom.slides.forEach((slide, slideIndex) => index !== slideIndex && slide.classList.remove('slide--active'))
        this.dom.slides[index].classList.add('slide--active')
        this.dom.navButtons.forEach(button => button.classList.remove('nav-link--active'))
        this.dom.navButtons[index].classList.add('nav-link--active')
        this.dom.navButtons[index].scrollIntoView({block: "center", inline: "center"})
        this.render()
    }

    swipeDetection(event){
        if(event.type === 'touchstart'){
            this.state.touchStartX = event.changedTouches[0].clientX
            this.state.touchMovesArray = []
            setTimeout(() => {
                if(this.state.touchStartX - this.state.touchMovesArray[this.state.touchMovesArray.length - 1] > 75){
                    if(this.dom.slides.length - 1 > this.state.activeSlide){
                        this.state.activeSlide += 1
                        this.navigateToSection(this.state.activeSlide)
                    }
                }else if(this.state.touchStartX - this.state.touchMovesArray[this.state.touchMovesArray.length - 1] < -75){
                    if(this.state.activeSlide > 0){
                        this.state.activeSlide -= 1
                        this.navigateToSection(this.state.activeSlide)
                    }
                }
                this.state.touchStartX = undefined
                this.state.touchMovesArray = []
            }, 250)
        }
        if(event.type === 'touchmove'){
            this.state.touchMovesArray.push(event.changedTouches[0].clientX)
        }
    }
}


const app = new App();